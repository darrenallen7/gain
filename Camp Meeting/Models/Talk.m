//
//  Talk.m
//  Camp Meeting
//
//  Created by Darren Allen on 17/11/2014.
//  Copyright (c) 2014 SECmedia. All rights reserved.
//

#import "Talk.h"

@implementation Talk
@dynamic title;
@dynamic abstract;

@end
