//
//  Talk.h
//  Camp Meeting
//
//  Created by Darren Allen on 17/11/2014.
//  Copyright (c) 2014 SECmedia. All rights reserved.
//

#import <Parse/Parse.h>

@interface Talk : PFObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *abstract;

@end
